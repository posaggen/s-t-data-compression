#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <queue>
#include <set>
#include <map>
#include <string>
#include <bitset>

typedef long long LL;
#define pb push_back
#define MPII make_pair<int, int>
#define PII pair<int, int>
#define sz(x) (int)x.size()

using namespace std;

template<class T> T abs(T x){if (x < 0) return -x; else return x;}

const double eps = 1e-10;
const double PI = acos(-1.0);

const int maxn = 3200;
vector<int> pointID;
char buf[20];
bool exist[maxn];
bool legal[maxn][maxn];
int n;
int f[maxn], pre[maxn];

struct Trace{
	char ch;
	int time;
	double lon, lat;
	double p1, p2, p3;
	int p4;
}point[maxn];

double Rad(double d){
    return d * PI / 180.0;
}

double Geodist(double lat1, double lon1, double lat2, double lon2){
	double radLat1 = Rad(lat1);
    double radLat2 = Rad(lat2);
    double delta_lon = Rad(lon2 - lon1);
	double top_1 = cos(radLat2) * sin(delta_lon);
    double top_2 = cos(radLat1) * sin(radLat2) - sin(radLat1) * cos(radLat2) * cos(delta_lon);
    double top = sqrt(top_1 * top_1 + top_2 * top_2);
    double bottom = sin(radLat1) * sin(radLat2) + cos(radLat1) * cos(radLat2) * cos(delta_lon);
    double delta_sigma = atan2(top, bottom);
    double distance = delta_sigma * 6378137.0;
    return distance;
}

double dist(Trace u, Trace v){
	return Geodist(u.lat, u.lon, v.lat, v.lon);
}

bool input(Trace &u){
	int x, y, z;
	char ch;
	if (scanf(" %c", &u.ch) != 1) return false;
	scanf("%s", buf);
	scanf("%d:%d:%d", &x, &y, &z);
	u.time = x * 10000 + y * 100 + z;
	scanf("%lf", &u.lon);
	scanf(" %c", &ch);
	scanf("%lf", &u.lat);
	scanf(" %c", &ch);
	int t = (int)(u.lon / 100.0);
	u.lon = t + (u.lon - t * 100.0) / 60.0;
	t = (int)(u.lat / 100.0);
	u.lat = t + (u.lat - t * 100.0) / 60.0;
	scanf("%lf%lf%lf%d", &u.p1, &u.p2, &u.p3, &u.p4);
	return true;
}

bool equal(double x, double y){
	if (x - eps < y && x + eps > y) return true; else return false;
}

double getError(Trace a, Trace b, Trace c){
	double lat, lon;
	lat = Rad(a.lat); lon = Rad(a.lon);
	double x1 = -cos(lat) * cos(lon), y1 = sin(lat), z1 = cos(lat) * sin(lon);
	lat = Rad(b.lat); lon = Rad(b.lon);
	double x2 = -cos(lat) * cos(lon), y2 = sin(lat), z2 = cos(lat) * sin(lon);
	lat = Rad(c.lat); lon = Rad(c.lon);
	double x3 = -cos(lat) * cos(lon), y3 = sin(lat), z3 = cos(lat) * sin(lon);
	double xn = y1 * z2 - z1 * y2, yn = z1 * x2 - x1 * z2, zn = x1 * y2 - y1 * x2;
	double len = sqrt(xn * xn + yn * yn + zn * zn);
	xn /= len; yn /= len; zn /= len;
	double s = abs(PI / 2.0 - acos(x3 * xn + y3 * yn + z3 * zn));
	return s * 6378137.0;
}

double measure(vector<int> pointID, string path){
	freopen(path.c_str(), "r", stdin);
	n = 0;
	while (input(point[n + 1])) ++n;
	for (int i = 1; i <= n; ++i) exist[i] = false;
	for (int i = 0; i < sz(pointID); ++i)
		exist[pointID[i]] = true;
	int now = 1, pre = 1;
	double ans = 0;
	for (int i = 1; i < n; ++i){
		if (now <= i){
			pre = now;
			while (now <= i || !exist[now]) ++now;
		}
		if (!exist[i]) ans = max(ans, getError(point[pre], point[now], point[i]));
	}
	return ans;
}

void dfs(int l, int r, vector<int> &vec){
	double ans = 0;
	int k = -1;
	for (int i = l + 1; i < r; ++i){
		double t = getError(point[l], point[r], point[i]);
		if (ans < t){
			ans = t;
			k = i;
		}
	}
	if (ans < 30) return;
	vec.push_back(k);
	dfs(l, k, vec);
	dfs(k, r, vec);
}

vector<int> TrajCompress_DP(string path){
	vector<int> vec;
	freopen(path.c_str(), "r", stdin);
	n = 0;
	while (input(point[n + 1])) ++n;
	vec.push_back(1);
	vec.push_back(n);
	dfs(1, n, vec);
	return vec;
}

vector<int> TrajCompress_DP2(string path){
	vector<int> vec;
	freopen(path.c_str(), "r", stdin);
	n = 0;
	while (input(point[n + 1])) ++n;
	for (int i = 1; i <= n; ++i){
		for (int j = i; j <= min(i + 201, n); ++j){
			legal[i][j] = true;
			for (int k = i + 1; k < j; ++k)
				if (getError(point[i], point[j], point[k]) > 30){
					legal[i][j] = false;
					break;
				}
		}
	}
	f[1] = 1;
	pre[1] = 0;
	for (int i = 2; i <= n; ++i){
		f[i] = f[i - 1] + 1;
		pre[i] = i - 1;
		for (int j = max(i - 200, 1); j < i - 1; ++j){
			double tmp = 0;
			if (legal[j][i] && f[j] + 1 < f[i]){
				f[i] = f[j] + 1;
				pre[i] = j;
			}
		}
//cout << "processing #" << i << ' ' << f[i] << endl;
	}
	int t = n;
	while (t){
		vec.push_back(t);
		t = pre[t];
	}
	return vec;
}

int main(){
//	pointID = TrajCompress_DP("2007-10-14-GPS.log");
	pointID = TrajCompress_DP2("2007-10-14-GPS.log");
	freopen("loss_compression.log", "w", stdout);
	printf("%d\n", sz(pointID));
	sort(pointID.begin(), pointID.end());
	for (int i = 0; i < sz(pointID); ++i)
		printf("%d\n", pointID[i]);
	return 0;
}

