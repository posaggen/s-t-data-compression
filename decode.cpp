#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <queue>
#include <set>
#include <map>
#include <string>
#include <bitset>
#include <fstream>

typedef long long LL;
#define pb push_back
#define MPII make_pair<int, int>
#define PII pair<int, int>
#define sz(x) (int)x.size()

using namespace std;

template<class T> T abs(T x){if (x < 0) return -x; else return x;}

int a[100];
ifstream sourcefile;
int time;
double lon, lat;
char ff;
int flag;
vector<int> ans;

void printTime(int time){
	printf("%02d:%02d:%02d ", time / 3600, time / 60 % 60, time % 60);
}

void decode(){
	char ch;
	int cc;
	int num = 0;
	ff = getchar();
	flag = ff;
	if (flag < 0) flag += 256;
	while (true){
		ch = getchar();
		if (ch < 0) cc = 256 + ch; else cc = ch;
		cc -= 32;
		if (cc == -22) break;
		a[++num] = cc;
	}
	int now = 0;
	ans.clear();
	for (int i = num; i; --i){
		now *= 224;
		now += a[i];
		while (now >= 12){
			ans.push_back(now % 12);
			now /= 12;
		}
	}
	if (now > 0) ans.push_back(now);

	if (flag & 128){
		putchar('Y');
	} else putchar('N');
	int len = 0;
	printf(" 14-10-07 ");
	while (ans[len] == 10) ++len;
	int u = 0;
	while (ans[len] < 10){
		u = u * 10 + ans[len];
		++len;
	}
	time += u;
	printTime(time);

	double v, t;

	while (ans[len] == 10) ++len;
	v = 0;
	while (ans[len] < 10){
		v = v * 10 + ans[len];
		++len;
	}
	t = 1;
	++len;
	while (ans[len] < 10){
		t *= 0.1;
		v += t * ans[len];
		++len;
	}
	if (flag & 64) v = -v;
	lat += v;
	printf("%.4f E ", lat);

	while (ans[len] == 10) ++len;
	v = 0;
	while (ans[len] < 10){
		v = v * 10 + ans[len];
		++len;
	}
	t = 1;
	++len;
	while (ans[len] < 10){
		t *= 0.1;
		v += t * ans[len];
		++len;
	}
	if (flag & 32) v = -v;
	lon += v;
	printf("%.4f N ", lon);

	if (flag & 16) putchar('-');

	++len;
	for (; len < sz(ans); ++len)
		if (ans[len] == 10) putchar(' '); else 
			if (ans[len] == 11) putchar('.'); else 
				printf("%d", ans[len]);
	if (ans[len - 2] == 11) putchar('0');
	if (ans[len - 1] == 11) printf("00");
	printf(" %02d\n", flag % 16);
}

int main(){
	freopen("final_compressed.log", "r", stdin);
	freopen("decode.out", "w", stdout);
	time = 0;
	lat = lon = 0;	
	for (int i = 0; i < 85; ++i){
		decode();
	}
	return 0;
}

