#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <queue>
#include <set>
#include <map>
#include <string>
#include <bitset>

typedef long long LL;
#define pb push_back
#define MPII make_pair<int, int>
#define PII pair<int, int>
#define sz(x) (int)x.size()

using namespace std;

template<class T> T abs(T x){if (x < 0) return -x; else return x;}

const double eps = 1e-8;
const int maxn = 5555;
bool bb[300];
double deltalon, deltalat;
int deltatime;
vector<int> ans;

struct Trace{
	char ch;
	int time;
	double lon, lat;
	string p1, p2, p3;
	int p4;
}point[maxn];

int n;
char buf[10];
string str, ss;
string ww;
char buffer[100];

void print(int n){
	putchar(n + 32);
}

bool input(Trace &u){
	int x, y, z;
	char ch;
	if (scanf(" %c", &u.ch) != 1) return false;
	scanf("%s", buf);
	scanf("%d:%d:%d", &x, &y, &z);
	u.time = x * 3600 + y * 60 + z;
	scanf("%lf", &u.lon);
	scanf(" %c", &ch);
	scanf("%lf", &u.lat);
	scanf(" %c", &ch);
	cin >> u.p1 >> u.p2 >> u.p3;
	scanf("%d", &u.p4);
	return true;
}

void getFirstBit(int i){
	int cc;
	if (point[i].ch == 'Y') cc = 1; else cc = 0;
	cc <<= 1;
	if (deltalon < -eps) cc |= 1;
	cc <<= 1;
	if (deltalat < -eps) cc |= 1;
	cc <<= 1;
	if (point[i].p1[0] == '-') cc |= 1;
	cc <<= 4;
	cc += point[i].p4;
	putchar(cc);
	bb[cc] = true;
}

void possess(){
	int len = sz(str);
	int now = 0;
	for (int i = len - 1; i >= 0; --i){
		now *= 12;
		int cc;
		if (str[i] >= '0' && str[i] <= '9') cc = str[i] - '0'; else
			if (str[i] == ' ') cc = 10; else cc = 11;
		now += cc;
		while (now >= 224){
			ans.push_back(now % 224);
			now /= 224;
		}
	}
	if (now > 0) ans.push_back(now);
	for (int i = 0; i < sz(ans); ++i){
		print(ans[i]);
	}
}


int main(){
	freopen("loss_compressed.log", "r", stdin);
	freopen("final_compressed.log", "w", stdout);
	n = 0;
	while (input(point[n + 1])) ++n;
	memset(bb, false, sizeof(bb));
	for (int i = 1; i <= 85; ++i){
		ans.clear();
		deltatime = point[i].time - point[i - 1].time;
		deltalon = point[i].lon - point[i - 1].lon;
		deltalat = point[i].lat - point[i - 1].lat;
		getFirstBit(i);
		str = " ";
		sprintf(buffer, "%d", deltatime); ss = buffer;
		str += ss + " ";
		sprintf(buffer, "%.4f", deltalon); ss = buffer;
		if (ss[0] == '-') ss = ss.substr(1);
		str += ss + " ";
		sprintf(buffer, "%.4f", deltalat); ss = buffer;
		if (ss[0] == '-') ss = ss.substr(1);
		str += ss + " ";
		ss = point[i].p1;
		if (ss[0] == '-') ss = ss.substr(1);
		str += ss + " ";
		ss = point[i].p2;
		if (ss[0] == '-') ss = ss.substr(1);
		str += ss + " ";
		ss = point[i].p3;
		if (ss[0] == '-') ss = ss.substr(1);
		str += ss;
		possess();
		print(-22);
		cerr << i << endl;
	}
	return 0;
}

